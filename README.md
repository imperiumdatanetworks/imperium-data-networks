Imperium Data Networks is a reseller of IT Network Hardware and Telecommunications products. We provide your employees with computers, phone systems, network solutions, hardware, software and services to ensure your systems are always up and running optimally.

Address: 5483 West Waters Ave, Ste 1200N, Tampa, FL 33634, USA
Phone: 813-450-5515
Website: https://imperiumdatanetworks.com
